package figury;
import java.awt.Polygon;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class szesciakat extends Figura{
	    public szesciakat(Graphics2D buffer, int delay, int width, int height) {
	        super(buffer, delay, width, height);
	        this.shape = new Polygon(new int[] {32,53,45,19,11,20}, new int[] {12,30,60,55,30,20}, 6);
	        this.aft = new AffineTransform();
	        this.area = new Area(shape);
	    }

	}

