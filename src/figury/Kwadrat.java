package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Area;


public class Kwadrat extends Figura {
	
public  Kwadrat(Graphics2D buffer, int delay, int width, int height){
	 super(buffer,delay,width,height);
	this.aft= new AffineTransform();
	this.shape = new Rectangle2D.Float(0,0,8,8);
	this.area = new Area(shape);
 }
}
